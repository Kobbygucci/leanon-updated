import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  currentUser : User;

  // The various menu items

  constructor(private database: DatabaseService,  private userService: UserService) {

    this.userService.user.subscribe((data)=>{
            
            this.currentUser = data;
        });

        this.currentUser = userService.getUser();
   }

  breakfast = [];

  lunch = [];

  dinner = [];

  snacks = [];

  total_carbohydrate;
  total_fat;
  total_protein;
  total_calories;
  total_breakfast;
  total_lunch;
  total_dinner;
  total_snacks;

  set_all() {
    this.lunch = this.database.lunch;
    this.breakfast = this.database.breakfast;
    this.dinner = this.database.supper;
    this.snacks = this.database.snacks;
    this.total_carbohydrate = this.database.total_carbohydrate();
    this.total_fat = this.database.total_fat();
    this.total_protein = this.database.total_protein();
    this.total_calories = this.database.total_calories();
    this.total_breakfast = this.database.total_breakfast();
    this.total_lunch = this.database.total_lunch();
    this.total_dinner = this.database.total_supper();
    this.total_snacks = this.database.total_snacks();
  }

  ngOnInit() {
    this.set_all();
    console.log(this.currentUser);
  }



  calculateCarbohydrates(){
    return Math.round((this.currentUser.dietary_reference_intake*0.50)/4)
  }

  calculateFats(){
    return Math.round((this.currentUser.dietary_reference_intake*0.30)/9)
  }

  calculateProtein(){
    return Math.round((this.currentUser.dietary_reference_intake*0.20)/4)
  }

 calculateRemainingcarbo(){
   return Math.round(this.calculateCarbohydrates() - this.total_carbohydrate)
 }

 calculateRemainingFats(){
   return Math.round(this.calculateFats() - this.total_fat)
 }

 calculateRemainingProtein(){
   return Math.round(this.calculateProtein() - this.total_protein)
 }


}
