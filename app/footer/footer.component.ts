import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  // The info to be displayed by the footer
  contact = [
    {icon: 'call green', text: '+233 542 849 844'},
    {icon: 'whatsapp green', text: '+233 542 849 844'},
    {icon: 'envelope', text: ' support@leanOn.com'}
  ];

  social = [
    {icon: 'twitter', text: 'Follow Us on Twitter'},
    {icon: 'facebook', text: 'LeanOn on Facebook'},
    {icon: 'google plus', text: 'Follow Us on Google+'},
    {icon: 'instagram', text: 'LeanOn\'s Instagram'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
