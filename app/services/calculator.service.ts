import { Injectable } from "@angular/core";
import { ActivityLevel } from "../models/activity-level.model";
import { Gender } from "../models/gender.model";


@Injectable()
export class CalculatorService{


    constructor(){

    }

    /**
     * 
     * @param weight 
     * @param height 
     */
    bmi(weight : number, height : number) {
        return Math.round(((weight / height) / height) * 10000);
    }
  
    /**
     * 
     * @param weight 
     * @param height 
     * @param gender 
     * @param age 
     */
    bmr(weight : number, height : number, gender : Gender, age : number) {
        if (gender === Gender.FEMALE) {
            return Math.round((10 * weight) + (6.25 * height) - (5 * age) - 161);
        }
        else {
            return Math.round((10 * weight) + (6.25 * height) - (5 * age) + 5);
        }
    }
  
    /**
     * 
     * @param activity_level 
     */
    activity(activity_level : ActivityLevel) {
    
        switch (activity_level) {
            case ActivityLevel.LITTLE_TO_NO_EXERCISE: 
                return 1.2;
            case ActivityLevel.LTTILE_EXERCISE:  
                return 1.375;
            case ActivityLevel.MODERATE_EXERCISE:  
                return 1.55;
            case ActivityLevel.HEAVY_EXERCISE:  
                return 1.725;
            case ActivityLevel.VERY_HEAVY_EXERCISE:  
                return 1.9;
            default: 
                return 1.2;
        }
    }
  
    /**
     * 
     * @param weight 
     * @param height 
     * @param gender 
     * @param age 
     * @param activity_level 
     */
    dri(weight : number, height : number, gender:Gender, age:number, activity_level: ActivityLevel) {
       return Math.round(this.bmr(weight, height, gender, age) * this.activity(activity_level));
    }
  

}