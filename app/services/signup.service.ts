import { Injectable,Inject } from '@angular/core';
import { Http,Response,Headers } from '@angular/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http/src/response';
import { AuthenticationCredentials } from '../models/authentication-credentials.model'
import { SIGN_UP_URL } from './url-constants';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { SignupBody } from '../models/signupbody.model';

/*** 
 *  @author crazzyghost
 *  @description provides all http services concerned with singup
 */
@Injectable()
export class SignupService{

    

    /*** 
     * @param http      an http instance used for routing
     * @param baseUrl   the base url so you don't have to change it all the time
     *                  It is defined in the main app module and can be injected every where
     * @param router    a router instance used to navigate 
     */
    constructor(private http: Http, @Inject("BASE_URL") private baseUrl: string){
        
    }

    /**
     * perfoms the singup operation
     * @param signupBody valid details needed for registering 
     */
    doSignUp(signupBody: SignupBody){
        //get the data in json format
        let data = JSON.stringify(signupBody);
        //prepare the url
        let url  = this.baseUrl + SIGN_UP_URL;
        //prepare headers
        let headers = new Headers({'Content-Type': 'application/json'}); 
        //make the request
        return this.http.post(url,data,{ headers:headers }).map(
            (res: Response)=> { 
                return res.json();
            } 
        );
    }



}