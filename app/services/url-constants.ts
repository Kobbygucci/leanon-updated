/***
 * Defines all route endpoints
 */
export const LOGIN_URL     : string = "/auth/login/";
export const SIGN_UP_URL    : string = "/auth/signup/";
 