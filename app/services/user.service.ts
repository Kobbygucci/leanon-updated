import { Injectable } from "@angular/core";
import { User } from "../models/user.model";
import { Subject } from "rxjs/Subject";


/**
 * @author crazzyghost
 * @description provides and manages the current user of the application
 * 
 */

 @Injectable()
export class UserService{

    /**
     * <p>
     * The user object is an observable subject.
     * Other components will depend on its internals that is why it is made an observable
     * If it is already set, its content (the actual current user) can be accessed by the 
     * {@link UserService#getUser()} method.
     * </p>
     * 
     * <p>
     * It's content can be updated by calling the {@link UserService#updateUser()} method or 
     * by accessing the field directly. @see LoginComponent#onLogin()
     * </p>
     */
    user : Subject<User>
    
    constructor(){
        this.user = new Subject<User>();
    }


    /**
     * Provide the currrent user
     */
    getUser(){
        return new User(JSON.parse(localStorage.getItem("user")))
    }

    /**
     * Update the current user
     */
    updateUser(user : User){
        //modify this to suit
        this.user.next(user)
    }
}