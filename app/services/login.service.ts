import { Injectable,Inject } from '@angular/core';
import { Http,Response,Headers } from '@angular/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http/src/response';
import { AuthenticationCredentials } from '../models/authentication-credentials.model'
import { LOGIN_URL } from './url-constants';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

/*** 
 *  @author crazzyghost
 *  @description provides all http services concerned with login
 */
@Injectable()
export class LoginService{

    

    /*** 
     * @param http      an http instance used for routing
     * @param baseUrl   the base url so you don't have to change it all the time
     *                  It is defined in the main app module and can be injected every where
     */
    constructor(private http: Http, @Inject("BASE_URL") private baseUrl: string){
        
    }

    /**
     * perfoms the login operation
     * @param authCredentials valid credentials needed for logging in
     */
    doLogin(authCredentials: AuthenticationCredentials){
        //get the data in json format
        let data    = JSON.stringify(authCredentials);
        //prepare the url
        let url     = this.baseUrl + LOGIN_URL;
        //prepare headers
        let headers = new Headers({'Content-Type': 'application/json'}); 
        //make the request
        return this.http.post(url,data,{ headers:headers }).map(
            (res: Response)=> { 
                return res.json();
            } 
        );
    }

    
    isLoggedIn(){
        return localStorage.getItem("token") !== null;
    }

    logout(){
        localStorage.clear();
    }



}