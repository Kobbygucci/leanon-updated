import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';


declare var Chart: any;

@Component({
  selector: 'app-calories',
  templateUrl: './calories.component.html',
  styleUrls: ['./calories.component.css']
})
export class CaloriesComponent implements OnInit {

currentUser : User;
 currentFood : number;

 /* This is suppose the receive data from the API.
     it uses it to draw a chart on its HTML template page
      */

  constructor(private database: DatabaseService, private userService: UserService ) {
     this.userService.user.subscribe((data)=>{
            
            this.currentUser = data;
        });

        this.currentUser = userService.getUser();
   }

    set_all() {
        this.currentFood = this.database.total_calories();
    }

  myChart;

  breakfast = this.database.total_breakfast();
  lunch = this.database.total_lunch();
  dinner = this.database.total_supper();
  snacks = this.database.total_snacks();


  config = {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [
          this.breakfast,
          this.lunch,
          this.dinner,
          this.snacks
        ],
        backgroundColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 205, 86)',
          'rgb(153, 102, 255)',
          'rgb(54, 162, 235)'
        ],
        label: 'Total Calories'
      }],
      labels: [
        'Breakfast',
        'Lunch',
        'Dinner',
        'Snacks'
      ]
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        text: 'Calories Chart'
      },
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
  };


  ngOnInit() {
    const ctx = document.getElementById('chart');
    this.myChart = new Chart(ctx, this.config);
     this.set_all();
   // console.log(this.database.total_lunch());
     console.log(this.currentFood);

  }

}
