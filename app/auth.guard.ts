import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { LoginService } from './services/login.service';

@Injectable()
export class AuthGuard implements CanActivate {

  /* importing UserService to check if the user is authenticated.
  *  if not, he is redirected to the login page
  */

//   constructor(private user: UserService) {

//   }

    constructor(private service:LoginService){

    }
    canActivate(
        next  : ActivatedRouteSnapshot,
        state : RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
        // return this.user.getLoggedIn();
        return this.service.isLoggedIn();
    }
}
