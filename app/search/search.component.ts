import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../database.service';
declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private database: DatabaseService) { }

  FoodForm;
  AddForm;

  food = [
    {value: 'search', name: 'Search for food'},
    {value: 'upload', name: 'Upload Image of food'},
    {value: 'scan', name: 'Scan Bar-Code'}
  ];

  search_result = [];


  ngOnInit() {
    $('.ui.dropdown').dropdown();

    // checking the users choice and changing the search accordingly

    $('#search_type').change(function(){
      const search_val = $('#search_type').val();
      if (search_val === 'search') {
        $('#search_box').attr('type', 'text');
      }else {
        $('#search_box').attr('type', 'file');
      }
    });

    // The search form

    this.FoodForm = new FormGroup({
      search_type: new FormControl(),
      search_box: new FormControl()
    });

    this.AddForm = new FormGroup({
      food_name: new FormControl(),
      energy: new FormControl(),
      meal : new FormControl()
    });
  }

  onSubmit(user) {
    console.log(user);
  }
  onSearch(user) {
    this.search_result = this.database.search(user.search_box);
  }

  info(value) {
    $('#title').html(value.name);
    $('#energy').val(value.energy);
    $('#food_name').val(value.name);

  }

  onUpload(food) {
    const foods = $('#food_name').val();
    const meal = $('#meal').val();
    this.database.upload(meal , (this.database.searchOne(foods)));
    alert('Food Add to Meal to ' + meal);
  }

}
