import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // initializing the carousel when the page loads
    $('.owl-carousel').owlCarousel({
      loop: true,
      items: 1,
      margin: 10,
      center: true,
      autoplay: true,
      dots: true
  });
  }

}
