import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-diary',
  templateUrl: './diary.component.html',
  styleUrls: ['./diary.component.css']
})
export class DiaryComponent implements OnInit {

// The navigation menu and links
  nav = [
    {name: 'Search for food', link: 'search'},
    {name: 'Add food to diary', link: 'food'}
  ];

  constructor(private router: Router) { }

  ngOnInit() {
    // loading the first child when initialized
    this.router.navigateByUrl('diary/food');
  }

}
