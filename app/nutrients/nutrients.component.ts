import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-nutrients',
  templateUrl: './nutrients.component.html',
  styleUrls: ['./nutrients.component.css']
})
export class NutrientsComponent implements OnInit {

  nutrients = [
    {id: 'protein', total: 17, goal: 95},
    {id: 'carbohydrate', total: 136, goal: 238},
    {id: 'fiber', total: 4, goal: 38},
  ];

  constructor() { }

  /* This is suppose the receive data from the API.
     it uses it to draw a chart on its HTML template page
      */

  ngOnInit() {
    $('#protein, #carbohydrate, #fiber').progress({
      label: 'ratio',
      text: {
        ratio: '{value} taken out of {total}'
      }
    });

    for (const p of this.nutrients){
      $('#' + p.id).progress('set total', p.goal).progress('set progress', p.total);
    }
  }

}
