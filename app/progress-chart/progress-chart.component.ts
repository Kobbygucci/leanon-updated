import { Component, OnInit } from '@angular/core';
declare var Chart: any;

@Component({
  selector: 'app-progress-chart',
  templateUrl: './progress-chart.component.html',
  styleUrls: ['./progress-chart.component.css']
})
export class ProgressChartComponent implements OnInit {

  /* This is suppose the receive data from the API.
     it uses it to draw a progress chart on its HTML template page
      */

  myData = [80, 80, 80, 80, 80, 80, 80];
  myLabel = ['Feb 02', 'Feb 03', 'Feb 04', 'Feb 05', 'Feb 06', 'Feb 07', 'Feb 08'];

  myChart;
  config = {
    type: 'line',
    data: {
        labels: this.myLabel,
        datasets: [{
            label: 'Weight',
            backgroundColor: 'rgb(245,137,56)',
            borderColor: 'rgb(255,0,0)',
            data: this.myData,
            fill: false,
        }]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Progress Chart'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        }
    }
};

  constructor() { }

  ngOnInit() {
    const ctx = document.getElementById('chart');
    this.myChart = new Chart(ctx, this.config);
  }

}
