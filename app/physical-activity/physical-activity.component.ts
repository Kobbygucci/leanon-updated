import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-physical-activity',
  templateUrl: './physical-activity.component.html',
  styleUrls: ['./physical-activity.component.css']
})
export class PhysicalActivityComponent implements OnInit {

  constructor() { }

  SearchForm; ChooseForm; UploadForm; CaloriesForm;

  // this passes values to the HTML page

  exercise = [
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jogging', value: 'jogging'},
    {name: 'Ampe', value: 'ampe'},
    {name: 'Jumping', value: 'jumping'}
  ];

  ngOnInit() {
    $('.ui.dropdown').dropdown();

    // The various forms on the page

    this.ChooseForm = new FormGroup({
      choose: new FormControl()
    });

    this.SearchForm = new FormGroup({
      search: new FormControl()
    });

    this.UploadForm = new FormGroup({
      upload: new FormControl()
    });

    this.CaloriesForm = new FormGroup({
      weight: new FormControl(),
      time: new FormControl()
    });
  }

  // receiving the entered values in the form and logging it to the console

  onSubmit(user) {
    console.log(user);
  }

}
