import { Injectable } from '@angular/core';

@Injectable()
export class DatabaseService {

  private database = [
    { id: 1, name: 'Soya bean+ dried+ raw', protein: 32, fat: 17, carbohydrate: 27.6, energy: 391.4  },
    { id: 2, name: 'Bambara groundnut+ dried+ raw ', protein: 20.1, fat: 5.9, carbohydrate: 58.9, energy: 369.1  },
    { id: 3, name: 'Groundnut(fresh)', protein: 15, fat: 25, carbohydrate: 12, energy: 332  },
    { id: 4, name: 'Groundnut(dry)', protein: 23.2, fat: 44.8, carbohydrate: 23, energy: 549  },
    { id: 5, name: 'Cowpea', protein: 22.5, fat: 1.4, carbohydrate: 61, energy: 338  },
    { id: 6, name: 'Pea', protein: 22.3, fat: 1.1, carbohydrate: 62, energy: 339  },
    { id: 7, name: 'Beans+ white+ dried', protein: 22.1, fat: 1.5, carbohydrate: 53.2, energy: 314.7  },
    { id: 8, name: 'Yam tuber+ raw', protein: 1.9, fat: 0.2, carbohydrate: 27.5, energy: 119.4  },
    { id: 9, name: 'Cassava+ tuber+ raw', protein: 1.2, fat: 0.3, carbohydrate: 35.6, energy: 149.9  },
    { id: 10, name: 'Cocoyam+ tuber+ raw ', protein: 2.4, fat: 0.2, carbohydrate: 27.4, energy: 121  },
    { id: 11, name: 'Irish Potato', protein: 1.7, fat: 0.1, carbohydrate: 18.9, energy: 82  },
    { id: 12, name: 'Potato+ raw', protein: 1.9, fat: 0.1, carbohydrate: 16.9, energy: 76.1  },
    { id: 13, name: 'Taro+ tuber+ raw', protein: 1.7, fat: 0.1, carbohydrate: 19, energy: 83.7  },
    { id: 14, name: 'Sweet potato+ pale yellow+ raw', protein: 1.5, fat: 0.2, carbohydrate: 25.3, energy: 109  },
    { id: 15, name: 'Plantain+ ripe+ raw', protein: 1.2, fat: 0.3, carbohydrate: 32, energy: 135.5  },
    { id: 16, name: 'Millet+ whole grain+ raw', protein: 10.9, fat: 4.1, carbohydrate: 62.6, energy: 330.9  },
    { id: 17, name: 'Sorghum+ whole grain+ raw ', protein: 10.5, fat: 3.3, carbohydrate: 63.1, energy: 324.1  },
    { id: 18, name: 'Wheat+ bran', protein: 15.9, fat: 4.8, carbohydrate: 23.2, energy: 199.6  },
    { id: 19, name: 'Maize+ yellow+ whole kernel+ dried+ raw', protein: 9, fat: 4.5, carbohydrate: 64.3, energy: 333.7  },
    { id: 20, name: 'Fonio+ black+ whole grain+ raw', protein: 8.9, fat: 3, carbohydrate: 69.4, energy: 340.2  },
    { id: 21, name: 'Rice+ brown+ raw', protein: 7.8, fat: 2.2, carbohydrate: 73.8, energy: 346.2  },
    { id: 22, name: 'Mango+ deep orange flesh', protein: 0.4, fat: 0.2, carbohydrate: 17.3, energy: 72.6  },
    { id: 23, name: 'Lemon+ raw', protein: 0.4, fat: 0.5, carbohydrate: 6.3, energy: 31.3  },
    { id: 24, name: 'Watermelon+ fruit+ raw', protein: 0.5, fat: 0.2, carbohydrate: 6.2, energy: 28.6  },
    { id: 25, name: 'Avocado+ pulp+ raw', protein: 1.7, fat: 14.7, carbohydrate: 1.4, energy: 144.7  },
    { id: 26, name: 'Baobab+ leaves+  raw', protein: 3.5, fat: 0.5, carbohydrate: 9.2, energy: 55.3  },
    { id: 27, name: 'Orange+ raw', protein: 0.7, fat: 0.3, carbohydrate: 8.9, energy: 41.1  },
    { id: 28, name: 'Cashew apple+ pulp+ raw', protein: 1, fat: 0.7, carbohydrate: 10.7, energy: 53.1  },
    { id: 29, name: 'Pineapple+ pulp+ raw', protein: 0.4, fat: 0.2, carbohydrate: 12, energy: 51.4  },
    { id: 30, name: 'Apple+ with skin+ raw', protein: 0.3, fat: 0.2, carbohydrate: 11.4, energy: 48.6  },
    { id: 31, name: 'Papaya+ fruit+ ripe+ raw', protein: 0.5, fat: 0.1, carbohydrate: 7.3, energy: 32.1  },
    { id: 32, name: 'Banana+ white flesh+ raw', protein: 1.2, fat: 0.2, carbohydrate: 22.1, energy: 95  },
    { id: 33, name: 'Guava+ fruit+ raw', protein: 1, fat: 0.4, carbohydrate: 9.5, energy: 45.6  },
    { id: 34, name: 'Coconut+ mature kernel+ fresh+ raw', protein: 3.6, fat: 36.9, carbohydrate: 6.2, energy: 371.3  },
    { id: 35, name: 'Tomatoes+ canned+ packed in juice', protein: 1, fat: 0.2, carbohydrate: 3, energy: 17.8  },
    { id: 36, name: 'Peppers+ chilli+ raw', protein: 1.9, fat: 0.3, carbohydrate: 7.6, energy: 40.7  },
    { id: 37, name: 'Eggplant+ raw', protein: 1.1, fat: 0.2, carbohydrate: 4.6, energy: 24.6  },
    { id: 38, name: 'Cucumber+ raw', protein: 0.7, fat: 0.1, carbohydrate: 2.5, energy: 13.7  },
    { id: 39, name: 'Baobab leaves+ dried', protein: 13.7, fat: 2.1, carbohydrate: 35.2, energy: 214.5  },
    { id: 40, name: 'Carrot+ raw', protein: 1, fat: 0.3, carbohydrate: 5.7, energy: 29.5  },
    { id: 41, name: 'Onion+ raw', protein: 1.1, fat: 0.1, carbohydrate: 6.9, energy: 32.9  },
    { id: 42, name: 'Pumpkin leaves+ raw', protein: 3.7, fat: 0.3, carbohydrate: 1.3, energy: 22.7  },
    { id: 43, name: 'Garlic+ raw', protein: 6.8, fat: 0.4, carbohydrate: 25, energy: 130.8  },
    { id: 44, name: 'Okra leaves+ raw', protein: 2.7, fat: 0.3, carbohydrate: 4.3, energy: 30.7  },
    { id: 45, name: 'Cabbage+ raw', protein: 1.6, fat: 0.1, carbohydrate: 3.8, energy: 22.5  },
    { id: 46, name: 'Lettuce+ raw', protein: 1, fat: 0.2, carbohydrate: 2.3, energy: 15  },
    { id: 47, name: 'Pumpkin+ squash+ raw', protein: 1, fat: 0.1, carbohydrate: 5.6, energy: 27.3  },
    { id: 48, name: 'Spinach+ raw', protein: 2.8, fat: 0.6, carbohydrate: 1.7, energy: 23.4  },
    { id: 49, name: 'Tea+ infusion', protein: 0.1, fat: 0, carbohydrate: 0, energy: 0.4  },
    { id: 50, name: 'Coffee+ instant+ powder', protein: 15.4, fat: 0.4, carbohydrate: 72.4, energy: 354.8  },
    { id: 51, name: 'Wheat+ bran', protein: 15.9, fat: 4.8, carbohydrate: 23.2, energy: 199.6  },
    { id: 52, name: 'Salt', protein: 0, fat: 0, carbohydrate: 0, energy: 0  },
    { id: 53, name: 'Bay leaf+ dried', protein: 7.6, fat: 8.4, carbohydrate: 48.7, energy: 300.8  },
    { id: 54, name: 'Cumin+ seed', protein: 17.8, fat: 22.3, carbohydrate: 33.7, energy: 406.7  },
    { id: 55, name: 'Anis seed', protein: 17.6, fat: 15.9, carbohydrate: 35.4, energy: 355.1  },
    { id: 56, name: 'Cinnamon+ ground', protein: 4, fat: 1.2, carbohydrate: 27.5, energy: 136.8  },
    { id: 57, name: 'Vinegar', protein: 0.4, fat: 0, carbohydrate: 0.4, energy: 3.2  },
    { id: 58, name: 'Ginger+ root+ raw', protein: 1.8, fat: 0.8, carbohydrate: 15.7, energy: 77.2  },
    { id: 59, name: 'Jam', protein: 0.4, fat: 0, carbohydrate: 69, energy: 260  },
    { id: 60, name: 'Sugar(brown)', protein: 0.2, fat: 0, carbohydrate: 88.8, energy: 344  },
    { id: 61, name: 'Sugar(white)', protein: 0, fat: 0, carbohydrate: 99.99, energy: 399.96  },
    { id: 62, name: 'Honey', protein: 0.4, fat: 0, carbohydrate: 81, energy: 325.6  },
    { id: 63, name: 'Sunflower', protein: 13, fat: 27.7, carbohydrate: 51.3, energy: 486  },
    { id: 64, name: 'Cashew nut+ raw', protein: 17.7, fat: 44.4, carbohydrate: 27, energy: 578.4  },
    { id: 65, name: 'Sesame seeds+ whole+ dried+ raw', protein: 18.2, fat: 48.9, carbohydrate: 10, energy: 552.9  },
    { id: 66, name: 'Melon seeds+ slightly salted+ raw', protein: 27.5, fat: 47.9, carbohydrate: 11.3, energy: 586.3  },
    { id: 67, name: 'Groundnut+ shelled+ dried+ raw', protein: 22.4, fat: 45.9, carbohydrate: 14.6, energy: 561.1  },
    { id: 68, name: 'Cheese(hard) from whole cow\'s milk', protein: 24, fat: 32, carbohydrate: 1.2, energy: 384  },
    { id: 69, name: 'Condensed unsweetened milk(cow)', protein: 7, fat: 8, carbohydrate: 10, energy: 140  },
    { id: 70, name: 'Whole cow powder(cow)', protein: 25.5, fat: 27.5, carbohydrate: 37.5, energy: 500  },
    { id: 71, name: 'Condensed sweetened milk(cow)', protein: 7.3, fat: 8, carbohydrate: 53.9, energy: 317  },
    { id: 72, name: 'Skimmed milk powder(cow)', protein: 36, fat: 1, carbohydrate: 51, energy: 357  },
    { id: 73, name: 'Whole cow milk', protein: 3.8, fat: 4.8, carbohydrate: 5.4, energy: 79  },
    { id: 74, name: 'Yoghurt+ whole milk+ plain', protein: 3.8, fat: 3.4, carbohydrate: 6.8, energy: 73  },
    { id: 75, name: 'Butter+ from cow\'s milk (without salt)', protein: 0.8, fat: 79.6, carbohydrate: 0.2, energy: 720.4  },
    { id: 76, name: 'Lard and other animal fats', protein: 0, fat: 100, carbohydrate: 0, energy: 902  },
    { id: 77, name: 'Fish liver oils', protein: 0, fat: 100, carbohydrate: 0, energy: 900  },
    { id: 78, name: 'Vegetable oil', protein: 0, fat: 110, carbohydrate: 0, energy: 884  },
    { id: 79, name: 'Red palm oil', protein: 0, fat: 100, carbohydrate: 0, energy: 900  },
    { id: 80, name: 'Margarine', protein: 0, fat: 85, carbohydrate: 0, energy: 765  },
    { id: 81, name: 'Sardines in oil+ canned (drained solids with bone)', protein: 24.7, fat: 13.5, carbohydrate: 0, energy: 220.3  },
    { id: 82, name: 'Barracuda+ raw', protein: 19, fat: 0.7, carbohydrate: 0, energy: 82.3  },
    { id: 83, name: 'Anchovy+ fillet+ raw', protein: 19.1, fat: 3.7, carbohydrate: 0, energy: 109.7  },
    { id: 84, name: 'Tuna+ raw', protein: 23.3, fat: 5.1, carbohydrate: 0, energy: 139.1  },
    { id: 85, name: 'Mackerel+ raw', protein: 19.7, fat: 5, carbohydrate: 0, energy: 123.8  },
    { id: 86, name: 'Mudfish+ raw', protein: 17, fat: 3, carbohydrate: 0, energy: 95  },
    { id: 87, name: 'Catfish+ raw', protein: 16.3, fat: 5.7, carbohydrate: 0, energy: 116.5  },
    { id: 88, name: 'Tilapia+ raw', protein: 18.8, fat: 2.7, carbohydrate: 0, energy: 99.5  },
    { id: 89, name: 'Perch+ Nile+ raw', protein: 19.9, fat: 2, carbohydrate: 0, energy: 97.6  },
    { id: 90, name: 'Dried Fish', protein: 63, fat: 6.3, carbohydrate: 0, energy: 309  },
    { id: 91, name: 'Snail', protein: 12, fat: 2, carbohydrate: 4, energy: 82  },
    { id: 92, name: 'African carp+ raw', protein: 15.7, fat: 0.6, carbohydrate: 0, energy: 68.2  },
    { id: 93, name: 'Rabbit+ flesh only+ raw', protein: 21.6, fat: 4.8, carbohydrate: 0, energy: 129.6  },
    { id: 94, name: 'Goat+ meat+ raw', protein: 17.5, fat: 10.6, carbohydrate: 0, energy: 165.4  },
    { id: 95, name: 'Beef+ meat+ lean+ boneless+ raw', protein: 21.7, fat: 4.3, carbohydrate: 0, energy: 125.5  },
    { id: 96, name: 'Lamb/mutton+ meat+ moderately fat+ raw', protein: 16.5, fat: 21.2, carbohydrate: 0, energy: 256.8  },
    { id: 97, name: 'Pork+ meat+ approx. 20 % fat+ boneless+ raw', protein: 16.8, fat: 22, carbohydrate: 0, energy: 265.2  },
    { id: 98, name: 'Chicken+ duck+turkey', protein: 19, fat: 7, carbohydrate: 0, energy: 139  },
    { id: 99, name: 'Corned beef', protein: 27.2, fat: 15, carbohydrate: 0, energy: 251  },
    { id: 100, name: 'Eggs(hen+duck)', protein: 27.2, fat: 15, carbohydrate: 0, energy: 140  },
    { id: 101, name: 'Kenkey(Ga)', protein: 3.5, fat: 0.6, carbohydrate: 25, energy: 115  },
    { id: 102, name: 'Kenkey(fante)', protein: 3.6, fat: 0.8, carbohydrate: 40, energy: 200  },
    { id: 103, name: 'Banku', protein: 2.4, fat: 0.4, carbohydrate: 23.8, energy: 108  },
    { id: 104, name: 'Aboloo', protein: 4.2, fat: 0.2, carbohydrate: 29.8, energy: 138  },
    { id: 105, name: 'Oblayoo', protein: 1.3, fat: 0, carbohydrate: 10.8, energy: 44  },
    { id: 106, name: 'Tuo zaafi', protein: 1.4, fat: 0.2, carbohydrate: 22.1, energy: 95  },
    { id: 107, name: 'Corn dough', protein: 5.5, fat: 0.3, carbohydrate: 44.8, energy: 204  },
    { id: 108, name: 'Maize porridge', protein: 1.8, fat: 0.8, carbohydrate: 15.6, energy: 76  },
    { id: 109, name: 'Roasted cornmeal(ablemamu)', protein: 9.8, fat: 2.5, carbohydrate: 74.7, energy: 360  },
    { id: 110, name: 'Boiled corn on the cob', protein: 4.1, fat: 2.8, carbohydrate: 40.8, energy: 199  },
    { id: 111, name: 'Roasted corn on the cob', protein: 8, fat: 4.8, carbohydrate: 79.2, energy: 381  },
    { id: 112, name: 'Oats', protein: 17.1, fat: 7, carbohydrate: 63.1, energy: 374  },
    { id: 113, name: 'Rice ball', protein: 7.2, fat: 0.4, carbohydrate: 79.6, energy: 350  },
    { id: 114, name: 'Rice porridge', protein: 7.2, fat: 0.4, carbohydrate: 79.6, energy: 350  },
    { id: 115, name: 'White rice', protein: 7.2, fat: 0.4, carbohydrate: 79.6, energy: 350  },
    { id: 116, name: 'Biscuits', protein: 2.1, fat: 3.8, carbohydrate: 21.4, energy: 123  },
    { id: 117, name: 'Bread', protein: 9.7, fat: 0.6, carbohydrate: 60.8, energy: 287  },
    { id: 118, name: 'Doughnut', protein: 6.3, fat: 10.1, carbohydrate: 42.2, energy: 276  },
    { id: 119, name: 'Atsormor', protein: 8.1, fat: 11.4, carbohydrate: 58.7, energy: 354  },
    { id: 120, name: 'fresh cassava', protein: 0.7, fat: 0.2, carbohydrate: 42, energy: 172  },
    { id: 121, name: 'Boiled cassava', protein: 0.6, fat: 0, carbohydrate: 36, energy: 150  },
    { id: 122, name: 'Roasted cassava', protein: 0.95, fat: 0.3, carbohydrate: 56.7, energy: 232  },
    { id: 123, name: 'Cassava fufu', protein: 0.6, fat: 0.2, carbohydrate: 33.5, energy: 140  },
    { id: 124, name: 'Cassava + plantain fufu', protein: 0.6, fat: 0.1, carbohydrate: 32.5, energy: 120  },
    { id: 125, name: 'Plantain fufu', protein: 0.96, fat: 0.2, carbohydrate: 34, energy: 140  },
    { id: 126, name: 'Roasted plantain', protein: 1.2, fat: 0.2, carbohydrate: 42.4, energy: 174  },
    { id: 127, name: 'Ripe plantain', protein: 1.1, fat: 0.2, carbohydrate: 38.8, energy: 160  },
    { id: 128, name: 'Fried plantain', protein: 1.4, fat: 1.5, carbohydrate: 51.6, energy: 223  },
    { id: 129, name: 'Boiled plantain', protein: 1.3, fat: 0.1, carbohydrate: 31.6, energy: 119  },
    { id: 130, name: 'Kokonte', protein: 1.4, fat: 0.4, carbohydrate: 82.8, energy: 340  },
    { id: 131, name: 'Gari', protein: 1.7, fat: 0.1, carbohydrate: 81, energy: 338  },
    { id: 132, name: 'Sweet potato', protein: 0.8, fat: 0.2, carbohydrate: 35.7, energy: 148  },
    { id: 133, name: 'cocoyam', protein: 2.7, fat: 0.2, carbohydrate: 36, energy: 158  },
    { id: 134, name: 'Yam', protein: 1.8, fat: 0.2, carbohydrate: 33.2, energy: 142  },
    { id: 135, name: 'Beans(cooked)', protein: 6.2, fat: 5.7, carbohydrate: 18.1, energy: 143  },
    { id: 136, name: 'Cowpeas', protein: 10.5, fat: 1.1, carbohydrate: 60.6, energy: 330  },
    { id: 137, name: 'Agushie', protein: 29.5, fat: 45.3, carbohydrate: 12, energy: 574  },
    { id: 138, name: 'Palmnuts', protein: 1.8, fat: 46.8, carbohydrate: 16.2, energy: 493  },
    { id: 139, name: 'Palmoil', protein: 0, fat: 100, carbohydrate: 0, energy: 900  },
    { id: 140, name: 'Palm kernel oil', protein: 0, fat: 100, carbohydrate: 0, energy: 900  },
    { id: 141, name: 'Coconut oil', protein: 0, fat: 100, carbohydrate: 0, energy: 900  },
    { id: 142, name: 'smoked dried fish', protein: 70, fat: 6, carbohydrate: 0, energy: 335  },
    { id: 143, name: 'Smoked tuna', protein: 31.8, fat: 0.3, carbohydrate: 0, energy: 139  },
    { id: 144, name: 'Smoked tilapia', protein: 67.5, fat: 8.8, carbohydrate: 3.8, energy: 368  },
    { id: 145, name: 'Palm nut soup', protein: 1.8, fat: 46.8, carbohydrate: 16.2, energy: 493  },
    { id: 146, name: 'Groundnut soup', protein: 5.7, fat: 15.1, carbohydrate: 0.2, energy: 147  },
    { id: 147, name: 'Okro soup', protein: 1.9, fat: 2.8, carbohydrate: 4.2, energy: 43  },
    { id: 148, name: 'Okro stew', protein: 2.8, fat: 11.6, carbohydrate: 5, energy: 122  },
    { id: 149, name: 'Tomato stew', protein: 1.9, fat: 13.9, carbohydrate: 0.5, energy: 123  },
    { id: 150, name: 'Hot pepper', protein: 1.1, fat: 0.1, carbohydrate: 10.7, energy: 42  },
    { id: 151, name: 'waakye', protein: 5.5, fat: 4.1, carbohydrate: 37.8, energy: 158  },
    { id: 152, name: 'Gari+beans+oil', protein: 4.6, fat: 25.1, carbohydrate: 20.4, energy: 325  },
    { id: 153, name: 'Aprapransa', protein: 3.6, fat: 6.4, carbohydrate: 18, energy: 136  }
];

  public breakfast = [];
  public lunch = [];
  public supper = [];
  public snacks = [];

  constructor() {

  }

  search(input) {
    const ans = [];
    for (let i = 0; i < this.database.length; i++) {
    if ((this.database[i].name).toLowerCase().includes(input.toLowerCase())) {
      ans.push(this.database[i]);
    }
  }
  return ans;
  }

  searchOne(input) {
    for (let i = 0; i < this.database.length; i++) {
    if ((this.database[i].name).toLowerCase().includes(input.toLowerCase())) {
      return this.database[i];
    }
  }
  return false;
  }

  upload(meal, obj) {
    if (meal === 'breakfast') {
      this.breakfast.push(obj);
    }else if (meal === 'lunch') {
      this.lunch.push(obj);
    }else if (meal === 'supper') {
    this.supper.push(obj);
    }else if (meal === 'snacks') {
      this.snacks.push(obj);
    }
  }

  total_carbohydrate() {
    const result = [];
    for (let i = 0; i < this.breakfast.length; i++) {
      result.push(this.breakfast[i].carbohydrate);
    }
    for (let i = 0; i < this.lunch.length; i++) {
      result.push(this.lunch[i].carbohydrate);
    }
    for (let i = 0; i < this.supper.length; i++) {
      result.push(this.supper[i].carbohydrate);
    }
    for (let i = 0; i < this.snacks.length; i++) {
      result.push(this.snacks[i].carbohydrate);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_fat() {
    const result = [];
    for (let i = 0; i < this.breakfast.length; i++) {
      result.push(this.breakfast[i].fat);
    }
    for (let i = 0; i < this.lunch.length; i++) {
      result.push(this.lunch[i].fat);
    }
    for (let i = 0; i < this.supper.length; i++) {
      result.push(this.supper[i].fat);
    }
    for (let i = 0; i < this.snacks.length; i++) {
      result.push(this.snacks[i].fat);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_protein() {
    const result = [];
    for (let i = 0; i < this.breakfast.length; i++) {
      result.push(this.breakfast[i].protein);
    }
    for (let i = 0; i < this.lunch.length; i++) {
      result.push(this.lunch[i].protein);
    }
    for (let i = 0; i < this.supper.length; i++) {
      result.push(this.supper[i].protein);
    }
    for (let i = 0; i < this.snacks.length; i++) {
      result.push(this.snacks[i].protein);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_calories() {
    const result = [];
    for (let i = 0; i < this.breakfast.length; i++) {
      result.push(this.breakfast[i].energy);
    }
    for (let i = 0; i < this.lunch.length; i++) {
      result.push(this.lunch[i].energy);
    }
    for (let i = 0; i < this.supper.length; i++) {
      result.push(this.supper[i].energy);
    }
    for (let i = 0; i < this.snacks.length; i++) {
      result.push(this.snacks[i].energy);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_breakfast() {
    const result = [];
    for (let i = 0; i < this.breakfast.length; i++) {
      result.push(this.breakfast[i].carbohydrate);
      result.push(this.breakfast[i].protein);
      result.push(this.breakfast[i].fat);
      result.push(this.breakfast[i].energy);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_lunch() {
    const result = [];
    for (let i = 0; i < this.lunch.length; i++) {
      result.push(this.lunch[i].carbohydrate);
      result.push(this.lunch[i].protein);
      result.push(this.lunch[i].fat);
      result.push(this.lunch[i].energy);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_supper() {
    const result = [];
    for (let i = 0; i < this.supper.length; i++) {
      result.push(this.supper[i].carbohydrate);
      result.push(this.supper[i].protein);
      result.push(this.supper[i].fat);
      result.push(this.supper[i].energy);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

  total_snacks() {
    const result = [];
    for (let i = 0; i < this.snacks.length; i++) {
      result.push(this.snacks[i].carbohydrate);
      result.push(this.snacks[i].protein);
      result.push(this.snacks[i].fat);
      result.push(this.snacks[i].energy);
    }
    let res = 0;
    if (result.length) {
      res = result.reduce(function (a, b){
        return a + b;
      });
    }
    return res;
  }

}
