import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { DatabaseService } from '../database.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-my-home',
  templateUrl: './my-home.component.html',
  styleUrls: ['./my-home.component.css']
})
export class MyHomeComponent implements OnInit {

    currentUser : User;
    currentFood : number;
    
    
    constructor(private userService: UserService, private database: DatabaseService) { 
        this.userService.user.subscribe((data)=>{
            
            this.currentUser = data;
        });

        this.currentUser = userService.getUser();
    }

    set_all() {
        this.currentFood = this.database.total_calories();
    }

    ngOnInit() {
        this.set_all();
        console.log(this.currentUser);
        console.log(this.currentFood);

    }

}
