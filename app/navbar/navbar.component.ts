import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nav = [
    {name: 'My Home', img: 'home', link: 'home', color: 'green'},
    {name: 'Physical Activity', img: 'heartbeat', link: 'physical-activity', color: 'red'},
    {name: 'Diary', img: 'book', link: 'diary', color: 'brown'},
    {name: 'Nutritional Report', img: 'pie chart', link: 'nutritional-report', color: 'violet'},
    {name: 'Progress Chart', img: 'line chart', link: 'progress-chart', color: 'blue'}
  ];

  constructor(private service: LoginService, private router: Router) { }

  /* function  to change the buttons on the navbar as we navigate the pages
     and redirect the user to the login page if the try visiting a page without
     login in
      */

  some() {
    if (this.service.isLoggedIn()) {
      $('#all').hide();
      $('#some').show();
    } else {
      const ask = confirm('You are not logged in, Click OK to take you to the login page');
      if (ask) {
        this.router.navigateByUrl('login');
      }
    }
  }
  all() {
    $('#all').show();
    $('#some').hide();
  }

  logOut() {
    this.service.logout();
  }

  ngOnInit() {
    $('#some').hide();
  }

}
