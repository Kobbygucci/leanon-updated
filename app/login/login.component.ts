import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { AuthenticationCredentials } from '../models/authentication-credentials.model'
import { UserService } from '../services/user.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor(private service: LoginService, public userService: UserService, private router: Router) { }

    LoginForm: FormGroup;

    ngOnInit() {
        $('.ui.rating').rating();
        this.LoginForm = new FormGroup({
            // validating user input
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        
        });
    }

    
    /***
     * Callback method performed when a user clicks login
     * @param data the form data to be used in the login process
     */
    onLogin(data) {
        let creds = new AuthenticationCredentials({
            username : data.username,
            password : data.password
        });

        let $this = this;

        this.service.doLogin(creds).subscribe(
            (res)=>{
                //save what needs to be saved
                // console.log(res.user);
                localStorage.setItem("user",JSON.stringify(res.user))
                localStorage.setItem("token",res.token);
                
                this.userService.user.next(this.userService.getUser())
                
                this.router.navigateByUrl('home');

            },
            (err)=>{
                console.log(err);
            }
        );
    }



}
