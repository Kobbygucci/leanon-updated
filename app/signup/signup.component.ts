import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateConfig } from '@angular/router/src/config';

import { SignupService } from '../services/signup.service';
import { User } from '../models/user.model';
import { WeightRecord } from '../models/weight-record.model';
import { CalculatorService } from '../services/calculator.service';
import { SignupBody } from '../models/signupbody.model';
import { Router } from '@angular/router';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

    SignUpForm;

  
    // the various step info
    step = [
        {id: 1, icon: 'user', title: 'Basic Information', description: 'These make you Unique', time: 'active'},
        {id: 2, icon: 'child', title: 'More Information', description: 'A little more detials', time: ''},
        {id: 3, icon: 'doctor', title: 'Health Information', description: 'Allows us to help you', time: ''}
    ];

    // this loads the form in steps when clicked
    cstep = function(id){
        $('#' + id).removeClass('active').addClass('completed');
        $('#' + (id + 1)).addClass('active');
        $('#step' + id).hide();
        $('#step' + (id + 1)).show();
    };

    bstep = function(id){
        $('#' + id).removeClass('active');
        $('#' + (id - 1)).removeClass('completed').addClass('active');
        $('#step' + id).hide();
        $('#step' + (id - 1)).show();
    };    

    constructor(private service: SignupService,private calculator: CalculatorService, private router: Router) { }

    ngOnInit() {
        $('.ui.dropdown').dropdown();
        $('#step3 ,#step2').hide();
        $('#date_of_birth').datepicker();

// the various forms on the page

        this.SignUpForm = new FormGroup({
            first_name          : new FormControl('', Validators.compose([Validators.required,
                                                            Validators.pattern('[\\w\\-\\s\\/]+')])),
            last_name           : new FormControl('', Validators.compose([Validators.required,
                                                            Validators.pattern('[\\w\\-\\s\\/]+')])),
            username            : new FormControl('', Validators.required),
            email               : new FormControl(),
            password            : new FormControl('', Validators.required),
            confirm_password    : new FormControl(),
            date_of_birth       : new FormControl(),
            gender              : new FormControl(),
            height              : new FormControl(),
            weight              : new FormControl(),
            activity_level      : new FormControl(),
            diabetes            : new FormControl(),
            heart_disease       : new FormControl(),
            high_cholesterol    : new FormControl(),
            high_blood_pressure : new FormControl()
        });

    }

    onSubmit(user) {
        console.log(user);
    }

    onSignUp(user) {

        let _weight_records = [ 
            new WeightRecord({
                date: new Date(),
                weight: user.weight
            })
        ]

        let _user = new User({
            activity_level: user.activity_level,
            age: 20,
            birthday_date: user.date_of_birth,
            bmi: this.calculator.bmi(user.weight, user.height),
            bmr: this.calculator.bmr(user.weight, user.height, user.gender, user.age),
            current_weight: user.weight,
            dietary_reference_intake: this.calculator.dri(user.weight, user.height, user.gender, 20, user.activity_level),
            email: user.email,
            first_name: user.first_name,
            gender: user.gender,
            has_diabetes: user.diabetes ? true : false,
            has_high_blood_pressure: user.high_blood_pressure ? true : false,
            has_high_cholesterol: user.high_cholesterol ? true : false,
            has_heart_disease: user.heart_disease ? true : false,
            height: user.height,
            last_name: user.last_name,
            password: user.password,
            start_weight: user.weight,
            username: user.username,
            weight_records : _weight_records
        });
         
        let body = new SignupBody({user: _user, weight_records: _weight_records});
        console.log(body);
        this.service.doSignUp(body).subscribe(
            (res)=>{
                console.log(res)
                this.router.navigateByUrl('login');
            },
            (err)=>{
                console.log(err)
            }
        );
     
    }
        

}
