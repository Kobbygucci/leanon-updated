import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
declare var Chart: any;

@Component({
  selector: 'app-macros',
  templateUrl: './macros.component.html',
  styleUrls: ['./macros.component.css']
})
export class MacrosComponent implements OnInit {

  /* This is suppose the receive data from the API.
  it uses it to draw a chart on its HTML template page
  */

  constructor(private database: DatabaseService) { }

  myChart;
  carbohydrates = this.database.total_carbohydrate();
  fat = this.database.total_fat();
  protein = this.database.total_protein();
  total = this.carbohydrates + this.fat + this.protein;

  config = {
    type: 'pie',
    data: {
      datasets: [{
        data: [
          this.carbohydrates,
          this.fat,
          this.protein
        ],
        backgroundColor: [
          'rgb(255, 184, 64)',
          'rgb(255, 46, 77)',
          'rgb(75, 255, 192)'
        ],
        label: 'Total Calories'
      }],
      labels: [
        'Carbohydrates',
        'Fat',
        'Protein',
      ]
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        text: 'Macros Chart'
      },
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
  };


  breakfast = [];

  lunch = [];

  dinner = [];

  snacks = [];

  total_carbohydrate;
  total_fat;
  total_protein;
  total_calories;
  total_breakfast;
  total_lunch;
  total_dinner;
  total_snacks;

  set_all() {
    this.lunch = this.database.lunch;
    this.breakfast = this.database.breakfast;
    this.dinner = this.database.supper;
    this.snacks = this.database.snacks;
    this.total_carbohydrate = this.database.total_carbohydrate();
    this.total_fat = this.database.total_fat();
    this.total_protein = this.database.total_protein();
    this.total_calories = this.database.total_calories();
    this.total_breakfast = this.database.total_breakfast();
    this.total_lunch = this.database.total_lunch();
    this.total_dinner = this.database.total_supper();
    this.total_snacks = this.database.total_snacks();
  }



  ngOnInit() {
     this.set_all();
    const ctx = document.getElementById('chart');
    this.myChart = new Chart(ctx, this.config);
  }

  calculateCarbohydratePercent(){
    return Math.round(((this.total_carbohydrate*4)/(this.total_carbohydrate*4 + this.total_fat*9 + this.total_protein*4))*100)
  }

   calculateFatPercent(){
    return Math.round(((this.total_fat*9)/(this.total_carbohydrate*4 + this.total_fat*9 + this.total_protein*4))*100)
  }

   calculateProteinPercent(){
    return Math.round(((this.total_protein*4)/(this.total_carbohydrate*4 + this.total_fat*9 + this.total_protein*4))*100)
  }

}
