import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Router } from '@angular/router';

/* This is responsible for storing and sharing data
   between the various pages */

@Injectable()
export class UserService {

  private loggedIn: boolean;
  private USER: object;

  constructor(private http: HttpClient, private router: Router) {
    this.loggedIn = false;
   }

   setLoggedIn() {
     this.loggedIn = true;
   }

   getLoggedIn(): boolean {
     return this.loggedIn;
   }

   setLogOut() {
     this.loggedIn = false;
   }

   getDetials(value) {
    this.USER = value;
   }

   getInfo() {
     return this.USER;
   }

   bmi(weight, height) {
      return ((weight / height) / height) * 10000;
   }

   bmr(weight, height, gender, age) {
     if (gender === 'FEMALE') {
        return (10 * weight) + (6.25 * height) - (5 * age) - 161;
     }
     else {
      return (10 * weight) + (6.25 * height) - (5 * age) + 5;
     }
   }

   activity(activity_level) {
     switch (activity_level) {
       case 'LITTLE_TO_NO_EXERCISE': return 1.2;
       case 'LTTILE_EXERCISE':  return 1.375;
       case 'MODERATE_EXERCISE':  return 1.55;
       case 'HEAVY_EXERCISE':  return 1.725;
       case 'VERY_HEAVY_EXERCISE':  return 1.9;
       default: return 1.2;
     }
   }

   dri(weight, height, gender, age, activity_level) {
     return this.bmr(weight, height, gender, age) * this.activity(activity_level);
   }



   authenticateUser(user) {
     this.http.post('http://127.0.0.1:8000/auth/login/', user).subscribe(
       res => {
          this.USER = res;
          this.setLoggedIn();
          this.router.navigateByUrl('home');
       }, (err: HttpErrorResponse) => {
         console.log(err.error);
         console.log(err.name);
         console.log(err.message);
         console.log(err.status);
       }
     );
   }

   registerUser(user) {
     const data: object = {
       user: {
          activity_level: user.activity_level,
          age: 20,
          birthday_date: user.date_of_birth,
          bmi: this.bmi(user.weight, user.height),
          bmr: this.bmr(user.weight, user.height, user.gender, 20),
          current_weight: user.weight,
          dietary_reference_intake: this.dri(user.weight, user.height, user.gender, 20, user.activity_level),
          email: user.email,
          first_name: user.first_name,
          gender: user.gender,
          has_diabetes: user.diabetes ? true : false,
          has_high_blood_pressure: user.high_blood_pressure ? true : false,
          has_high_cholesterol: user.high_cholesterol ? true : false,
          has_heart_disease: user.heart_disease ? true : false,
          height: user.height,
          last_name: user.last_name,
          password: user.password,
          start_weight: user.weight,
          username: user.username
       }, weight_records: [{
          date: new Date(),
          weight: user.weight
       }]
     };
    this.http.post('http://127.0.0.1:8000/auth/signup/', data).subscribe(
      res => {
        console.log(res);
        console.log(data);
        console.log(user);
        this.router.navigateByUrl('login');
      }, (err: HttpErrorResponse) => {
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status);
        console.log(data);
        console.log(user);
      }
    );
  }

}
