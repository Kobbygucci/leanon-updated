import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-nutritional-report',
  templateUrl: './nutritional-report.component.html',
  styleUrls: ['./nutritional-report.component.css']
})
export class NutritionalReportComponent implements OnInit {

  nav = [
    {name: 'Calories', link: 'calories'},
    {name: 'Nutrients', link: 'nutrients'},
    {name: 'Macros', link: 'macros'}
  ];

  constructor(private router: Router) { }

  ngOnInit() {
    // redirecting to the first child
    this.router.navigateByUrl('nutritional-report/calories');
    $('#date').datepicker().datepicker('setDate', '0');
  }

}
