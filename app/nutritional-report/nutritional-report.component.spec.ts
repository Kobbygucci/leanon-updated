import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionalReportComponent } from './nutritional-report.component';

describe('NutritionalReportComponent', () => {
  let component: NutritionalReportComponent;
  let fixture: ComponentFixture<NutritionalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutritionalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
