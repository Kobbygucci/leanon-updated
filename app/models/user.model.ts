import{ ActivityLevel } from './activity-level.model';
import{ Gender } from './gender.model';
import { WeightRecord } from './weight-record.model';

/**
 * A user of LeanOn
 */
export class User{

    activity_level              : ActivityLevel;
    age                         : number;
    birthday_date               : string;
    bmi                         : number;
    bmr                         : number;
    current_weight              : number;
    dietary_reference_intake    : number;
    email                       : string;
    first_name                  : string;
    gender                      : Gender;
    has_diabetes                : boolean;
    has_high_blood_pressure     : boolean;
    has_high_cholesterol        : boolean;
    has_heart_disease           : boolean;
    height                      : number;
    last_name                   : number;
    password                    : string;
    start_weight                : number;
    username                    : string;
    weightRecords               : WeightRecord[];

    constructor(obj? : any){
        this.activity_level             = obj && obj.activity_level          || ActivityLevel.LITTLE_TO_NO_EXERCISE;
        this.age                        = obj && obj.age                     || 0;
        this.birthday_date              = obj && obj.birthday_date           || null;
        this.bmi                        = obj && obj.bmi                     || 0;
        this.bmr                        = obj && obj.bmr                     || 0;
        this.current_weight             = obj && obj.current_weight          || 0;
        this.dietary_reference_intake   = obj && obj.dietary_reference_intake|| 0;
        this.email                      = obj && obj.email                   || null;
        this.first_name                 = obj && obj.first_name              || null;
        this.gender                     = obj && obj.gender                  || Gender.MALE;
        this.has_diabetes               = obj && obj.has_diabetes            || false;
        this.has_high_blood_pressure    = obj && obj.has_high_blood_pressure || false;
        this.has_high_cholesterol       = obj && obj.has_high_cholesterol    || false;
        this.has_heart_disease          = obj && obj.has_heart_disease       || false;
        this.height                     = obj && obj.height                  || 0;
        this.last_name                  = obj && obj.last_name               || null;
        this.password                   = obj && obj.password                || null;
        this.start_weight               = obj && obj.start_weight            || 0;
        this.username                   = obj && obj.username                || null;
        this.weightRecords              = obj && obj.weightRecords           || [];
    }
}