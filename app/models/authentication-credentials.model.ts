/*** 
 *  @author crazzyghost
 *  @description embodies the credentials needed for authenticating a user
 */
export class AuthenticationCredentials{

    username: string;
    password: string;
    
    constructor(obj? : any){
        this.username = obj && obj.username || null;
        this.password = obj && obj.password || null;
    }
}