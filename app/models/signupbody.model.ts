import { User } from "./user.model";
import { WeightRecord } from "./weight-record.model";

export class SignupBody{

    user: User;
    weight_records : WeightRecord[];

    constructor(obj?:any){
        this.user            = obj && obj.user            || null;
        this.weight_records  = obj && obj.weight_records  || []
    }


}