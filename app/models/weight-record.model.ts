/**
 * A weight record
 */
export class WeightRecord{
    /**
     * The date on which this measurement was takes
     */
    date    : Date;
    /**
     * The actual weight measurement
     */
    weight  : number;
    
    constructor(obj? : any){
        this.date   = obj && obj.date   || new Date();
        this.weight = obj && obj.weight || 0;
    }
}