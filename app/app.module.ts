// Importing module for the app
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from "@angular/http";


// Importing the various created components
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { PhysicalActivityComponent } from './physical-activity/physical-activity.component';
import { DiaryComponent } from './diary/diary.component';
import { NutritionalReportComponent } from './nutritional-report/nutritional-report.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProgressChartComponent } from './progress-chart/progress-chart.component';
import { CaloriesComponent } from './calories/calories.component';
import { NutrientsComponent } from './nutrients/nutrients.component';
import { MacrosComponent } from './macros/macros.component';
import { AuthGuard } from './auth.guard';
import { SearchComponent } from './search/search.component';
import { FoodComponent } from './food/food.component';
import { SummaryComponent } from './summary/summary.component';
import { MyHomeComponent } from './my-home/my-home.component';


//services
// import { UserService } from './user.service';
import { UserService } from './services/user.service';
import { DatabaseService } from './database.service';
import { LoginService } from './services/login.service';
import { SignupService } from './services/signup.service';
import { CalculatorService } from './services/calculator.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarouselComponent,
    FooterComponent,
    PhysicalActivityComponent,
    DiaryComponent,
    NutritionalReportComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    NotFoundComponent,
    ProgressChartComponent,
    CaloriesComponent,
    NutrientsComponent,
    MacrosComponent,
    SearchComponent,
    FoodComponent,
    SummaryComponent,
    MyHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    // the main routing and child route for the app and guarding some pages
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'home', component: MyHomeComponent, canActivate: [AuthGuard]},
      {path: 'physical-activity', component: PhysicalActivityComponent, canActivate: [AuthGuard]},
      {path: 'diary', component: DiaryComponent, canActivate: [AuthGuard], children: [
        {path: 'search', component: SearchComponent},
        {path: 'food', component: FoodComponent}
      ]},
      {path: 'nutritional-report', component: NutritionalReportComponent, canActivate: [AuthGuard], children: [
        {path: 'calories', component: CaloriesComponent},
        {path: 'nutrients', component: NutrientsComponent},
        {path: 'macros', component: MacrosComponent}
      ]},
      {path: 'progress-chart', component: ProgressChartComponent, canActivate: [AuthGuard]},
      {path: 'login', component: LoginComponent},
      {path: 'signup', component: SignupComponent},
      {path: '**', component: NotFoundComponent}
    ])
  ],
  providers: [
      UserService, 
      AuthGuard, 
      DatabaseService,
      LoginService,
      SignupService,
      CalculatorService,
      {provide:'BASE_URL',useValue:'http://127.0.0.1:8000'}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
